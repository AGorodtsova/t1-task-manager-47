package ru.t1.gorodtsova.tm.service;

import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {
/*
    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        projectRepository.add(USER1_PROJECT_LIST);
        taskRepository.add(USER1_TASK_LIST);
    }

    @After
    public void after() {
        projectRepository.removeAll(USER1_PROJECT_LIST);
        taskRepository.removeAll(USER1_TASK_LIST);
    }

    @Test
    public void bindTaskToProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), USER1_TASK1.getProjectId());
        Assert.assertNotEquals(USER1_PROJECT2.getId(), USER1_TASK1.getProjectId());

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.bindTaskToProject(null, USER1_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), null, USER1_TASK1.getId());

        thrown.expect(TaskIdEmptyException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER2_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(TaskNotFoundException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER2_TASK1.getId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNotNull(USER1_TASK1.getProjectId());
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(null, USER1_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), null, USER1_TASK1.getId());

        thrown.expect(TaskIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER2_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(TaskNotFoundException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER2_TASK1.getId());
    }

    @Test
    public void removeProjectById() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK2.getId());
        projectTaskService.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.removeProjectById(null, USER1_PROJECT1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.removeProjectById(USER1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.removeProjectById(USER1.getId(), USER2_PROJECT1.getId());
    }*/

}
